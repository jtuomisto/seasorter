#include "seasorter-private.h"


/* Internal variables*/
const sort_algo seasorter_algos[] = {
    {"bubble", bubblesort},
    {"quick", quicksort},
    {"shell", shellsort},
    {"insertion", insertionsort},
    {"oddeven", oddevensort},
    {"cocktail", cocktailsort},
    {"comb", combsort},
    {"cycle", cyclesort},
    {"heap", heapsort},
    {"selection", selectionsort},
    {"merge", mergesort},
    {"intro", introsort},
    {NULL, NULL}
};


/* Internal functions*/
void magick_fail(MagickWand *wand) {
    char *msg;
    ExceptionType severity;

    msg = MagickGetException(wand, &severity);
    fprintf(stderr, "%s\n", msg);
    MagickRelinquishMemory(msg);
}


inline num_t get_clamped_color(MagickRealType color) {
    return (num_t) (color / QuantumRange * (MagickRealType) 255);
}

inline MagickRealType get_quantum_color(num_t color) {
    return ((MagickRealType) color / (MagickRealType) 255 * QuantumRange);
}

inline num_t pack_clamped(num_t r, num_t g, num_t b) {
    return (r << 24) | (g << 16) | (b << 8);
}

inline void unpack_clamped(num_t color, num_t *r, num_t *g, num_t *b) {
    *r = 0x000000FF & (color >> 24);
    *g = 0x000000FF & (color >> 16);
    *b = 0x000000FF & (color >> 8);
}


int index_comparator(void *lhs, void *rhs) {
    size_t a = *(size_t*) lhs, b = *(size_t*) rhs;
    return COMPARE(a, b);
}

int color_comparator(void *lhs, void *rhs) {
    num_t a = *(num_t*) lhs, b = *(num_t*) rhs;
    num_t r_a, g_a, b_a, r_b, g_b, b_b;
    float luma_a, luma_b;

    unpack_clamped(a, &r_a, &g_a, &b_a);
    unpack_clamped(b, &r_b, &g_b, &b_b);
    luma_a = 0.2126 * r_a + 0.7152 * g_a + 0.0722 * b_a;
    luma_b = 0.2126 * r_b + 0.7152 * g_b + 0.0722 * b_b;

    return a == b ? 0 : COMPARE(luma_a, luma_b);
}


void add_callback(void *data, bool is_finished) {
    (*(size_t*) data)++;
}

void img_callback(void *data, bool is_finished) {
    callback_data *cbdata = (callback_data*) data;
    PixelIterator *iter;
    PixelWand **pixels;
    PixelWand *bg;
    PixelInfo pixel;
    size_t index;
    num_t r, g, b;
    size_t mod = cbdata->counter++ % cbdata->new_frame;

    if(!cbdata->success || (mod != 0 && !is_finished))
        return;

    printf(".");
    fflush(stdout);

    bg = NewPixelWand();
    PixelSetColor(bg, "black");

    if(cbdata->separate) {
        cbdata->result = NewMagickWand();
        cbdata->frame++;
    }

    MagickNewImage(cbdata->result, cbdata->width, cbdata->height, bg);
    MagickNextImage(cbdata->result);

    iter = NewPixelIterator(cbdata->result);

    for(size_t y = 0; y < cbdata->height; y++) {
        pixels = PixelGetNextIteratorRow(iter, &index);

        for(size_t x = 0; x < cbdata->width; x++) {
            index = y * cbdata->width + x;

            if(cbdata->pixel_pos != NULL)
                index = cbdata->pixel_pos[index];

            PixelGetQuantumPacket(pixels[x], &pixel);
            unpack_clamped(cbdata->pixel_data[index], &r, &g, &b);

            pixel.red   = get_quantum_color(r);
            pixel.green = get_quantum_color(g);
            pixel.blue  = get_quantum_color(b);

            PixelSetPixelColor(pixels[x], &pixel);
        }

        PixelSyncIterator(iter);
    }

    DestroyPixelWand(bg);
    DestroyPixelIterator(iter);

    if(cbdata->separate) {
        cbdata->success = write_png(cbdata->output, cbdata->frame, cbdata->result);
        DestroyMagickWand(cbdata->result);
    }
}


num_t* get_pixel_data(const char *input, size_t *w, size_t *h) {
    MagickWand *original = NewMagickWand();
    PixelIterator *iter;
    PixelWand **pixels;
    PixelInfo pixel;
    num_t *pixel_data;
    size_t width, height, index_temp;

    printf("Opening image: %s\n", input);

    if(!MagickReadImage(original, input)) {
        magick_fail(original);
        DestroyMagickWand(original);
        return NULL;
    }

    width = MagickGetImageWidth(original);
    height = MagickGetImageHeight(original);
    pixel_data = malloc(width * height * sizeof(num_t));

    if(pixel_data == NULL) {
        fprintf(stderr, "Couldn't allocate memory for pixel data\n");
        DestroyMagickWand(original);
        return NULL;
    }

    iter = NewPixelIterator(original);
    *w = width;
    *h = height;

    printf("Loading pixels...\n");

    for(size_t y = 0; y < height; y++) {
        pixels = PixelGetNextIteratorRow(iter, &index_temp);

        for(size_t x = 0; x < width; x++) {
            index_temp = y * width + x;

            PixelGetQuantumPacket(pixels[x], &pixel);
            pixel_data[index_temp] = pack_clamped(get_clamped_color(pixel.red),
                                                  get_clamped_color(pixel.green),
                                                  get_clamped_color(pixel.blue));
        }
    }

    DestroyPixelIterator(iter);
    DestroyMagickWand(original);

    return pixel_data;
}

size_t* get_pixel_pos(size_t w, size_t h) {
    size_t *pixel_pos, temp;

    pixel_pos = malloc(w * h * sizeof(size_t));

    if(pixel_pos == NULL) {
        fprintf(stderr, "Couldn't allocate memory for pixel positions\n");
        return NULL;
    }

    for(size_t y = 0; y < h; y++) {
        for(size_t x = 0; x < w; x++) {
            temp = y * w + x;
            pixel_pos[temp] = temp;
        }
    }

    return pixel_pos;
}


size_t calculate_operations(void *array, size_t length, size_t size, comparator_func_t comparator, sort_func_t algo) {
    size_t operations = 0;
    void *copy;

    printf("Calculating...\n");

    copy = malloc(length * size);
    memcpy(copy, array, length * size);
    algo(copy, length, size, comparator, add_callback, &operations);
    free(copy);

    return operations;
}


bool write_gif(const char *output, MagickWand *wand) {
    MagickBooleanType status;
    char *fixed_output = NULL;
    const char *out;

    if(!string_ends_with(output, ".gif"))
        fixed_output = string_concat(2, output, ".gif");

    out = fixed_output != NULL ? fixed_output : output;

    printf("Writing GIF: %s\n", out);

    MagickSetOption(wand, "loop", "1");
    status = MagickWriteImages(wand, out, MagickTrue);

    if(fixed_output != NULL)
        free(fixed_output);

    if(status != MagickTrue)
        magick_fail(wand);

    return status == MagickTrue;
}

bool write_png(const char *directory, size_t frame, MagickWand *wand) {
    static const char *format = "%ld.png";
    size_t buf_size = snprintf(NULL, 0, format, frame) + 1;
    char file[buf_size], *path;
    MagickBooleanType status;

    snprintf(file, buf_size, format, frame);
    path = path_join(2, directory, file);

    MagickSetOption(wand, "quality", "100");
    status = MagickWriteImage(wand, path);

    free(path);

    if(status != MagickTrue)
        magick_fail(wand);

    return status == MagickTrue;
}


bool create_dir(const char *path) {
    struct stat info;
    int success = stat(path, &info);

    if(success != 0) {
        if(errno != ENOENT) {
            fprintf(stderr, "File system access error: %s\n", path);
            return false;
        }
        else if(mkdir(path, DIR_PERM) != 0) {
            fprintf(stderr, "Couldn't create directory: %s\n", path);
            return false;
        }

        printf("Created directory: %s\n", path);
        return true;
    }
    else if(!S_ISDIR(info.st_mode)) {
        fprintf(stderr, "Not a directory: %s\n", path);
        return false;
    }

    return true;
}


void seasorter_cleanup() {
    MagickWandTerminus();
}


/* Public functions */
void seasorter_init() {
    MagickWandGenesis();
    atexit(seasorter_cleanup);
}

const sort_algo* seasorter_get_algo(const char *algo) {
    if(algo == NULL)
        return NULL;

    for(int i = 0;; i++) {
        if(seasorter_algos[i].name == NULL)
            break;

        if(strcmp(algo, seasorter_algos[i].name) == 0)
            return &seasorter_algos[i];
    }

    return NULL;
}

bool seasorter_sort(const sort_options *options) {
    num_t *pixel_data;
    size_t *pixel_pos = NULL, width, height, length, operations;
    bool success;
    callback_data cbdata;

    if(options->separate) {
        if(!create_dir(options->output))
            return false;

        printf("Frames will be written to: %s\n", options->output);
    }

    pixel_data = get_pixel_data(options->input, &width, &height);
    length = width * height;

    if(pixel_data == NULL)
        return false;

    if(!options->colors) {
        pixel_pos = get_pixel_pos(width, height);

        if(pixel_pos == NULL) {
            free(pixel_data);
            return false;
        }

        shuffle(pixel_pos, length, sizeof(size_t));
        operations = calculate_operations(pixel_pos, length, sizeof(size_t), index_comparator, options->algo->func);
    }
    else {
        operations = calculate_operations(pixel_data, length, sizeof(num_t), color_comparator, options->algo->func);
    }

    cbdata.result = options->separate ? NULL : NewMagickWand();
    cbdata.output = options->output;
    cbdata.pixel_data = pixel_data;
    cbdata.pixel_pos = pixel_pos;
    cbdata.width = width;
    cbdata.height = height;
    cbdata.counter = 0;
    cbdata.frame = 0;
    cbdata.new_frame = 1;
    cbdata.separate = options->separate;
    cbdata.success = true;

    if(operations > options->frames)
        cbdata.new_frame = operations / options->frames;

    printf("Sorting pixels");

    if(options->colors)
        options->algo->func(pixel_data, length, sizeof(num_t), color_comparator, img_callback, &cbdata);
    else
        options->algo->func(pixel_pos, length, sizeof(size_t), index_comparator, img_callback, &cbdata);

    free(pixel_data);

    if(pixel_pos != NULL)
        free(pixel_pos);

    printf("\n");

    if(options->separate) {
        return cbdata.success;
    }
    else {
        success = write_gif(options->output, cbdata.result);
        DestroyMagickWand(cbdata.result);
        return success;
    }
}
