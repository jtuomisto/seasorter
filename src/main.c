#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <stdbool.h>
#include <time.h>
#include <getopt.h>
#include <errno.h>
#include "seasorter.h"


const char *EXENAME;


void print_short_usage();
void print_usage();
void print_error(const char *msg, ...);
void parse_options(sort_options *options, int argc, char **argv);


int main(int argc, char **argv) {
    sort_options options;

    EXENAME = argv[0];

    srand(time(NULL));
    parse_options(&options, argc, argv);
    seasorter_init();

    return seasorter_sort(&options) ? 0 : 1;
}


void print_short_usage() {
    printf("\nUsage: %s [OPTION...] INPUT_FILE\n", EXENAME);
    printf("Run `%s --help` for more information.\n", EXENAME);
}

void print_usage() {
    printf("Usage: %s [OPTION...] INPUT_FILE\n", EXENAME);
    printf("\nOptions:\n"
           "-o FILE, --output FILE\n"
           "    The output file (required)\n"
           "-a ALGO, --algorithm ALGO\n"
           "    The sorting algorithm (default: quick)\n"
           "-f FRAMES, --frames FRAMES\n"
           "    The amount of frames to render (default: 100)\n"
           "-c, --colors\n"
           "    Change sorting mode to sort colors instead.\n"
           "    The default is to randomize the positions of pixels,\n"
           "    and then sort them back to correct positions.\n"
           "-s, --separate\n"
           "    Frames are saved to separate files, instead of a gif.\n"
           "    Output file is the directory path where to save them.\n"
           "    The directory will be created, if it doesn't exist.\n"
           "    NOTE: only the last component of the path will be created,\n"
           "          the whole directory structure will *not* be.\n"
           "\nAlgorithms:\n"
           "bubble\n"
           "quick\n"
           "shell\n"
           "insertion\n"
           "oddeven\n"
           "cocktail\n"
           "comb\n"
           "cycle\n"
           "heap\n"
           "selection\n"
           "merge\n"
           "intro\n");
}

void print_error(const char *format, ...) {
    va_list args;

    va_start(args, format);
    vfprintf(stderr, format, args);
    va_end(args);

    print_short_usage();
}

void parse_options(sort_options *options, int argc, char **argv) {
    char shortopts[] = "ho:a:f:cs";
    struct option longopts[] = {
        {"help", no_argument, NULL, 'h'},
        {"output", required_argument, NULL, 'o'},
        {"algorithm", required_argument, NULL, 'a'},
        {"frames", required_argument, NULL, 'f'},
        {"colors", no_argument, NULL, 'c'},
        {"separate", no_argument, NULL, 's'},
        {0, 0, 0, 0}
    };
    int current, optindex, temp;
    char *algo_name = NULL;

    options->input = NULL;
    options->output = NULL;
    options->algo = NULL;
    options->frames = 100;
    options->separate = false;
    options->colors = false;

    while((current = getopt_long(argc, argv, shortopts, longopts, &optindex)) != -1) {
        switch(current) {
            case 'h':
                print_usage();
                exit(0);
            case 'o':
                options->output = optarg;
                break;
            case 'a':
                algo_name = optarg;
                break;
            case 'f':
                temp = strtoul(optarg, NULL, 0);

                if(errno == ERANGE || temp < 2) {
                    print_error("Invalid amount for frames: %s\n", optarg);
                    exit(1);
                }

                options->frames = temp;
                break;
            case 's':
                options->separate = true;
                break;
            case 'c':
                options->colors = true;
                break;
            case '?':
            default:
                print_short_usage();
                exit(1);
        }
    }

    if(optind < argc)
        options->input = argv[optind];

    if(options->input == NULL) {
        print_error("No input file given as an argument\n");
        exit(1);
    }

    if(options->output == NULL) {
        print_error("No output file given (-o/--output)\n");
        exit(1);
    }

    if(algo_name == NULL)
        algo_name = "quick";

    options->algo = seasorter_get_algo(algo_name);

    if(options->algo == NULL) {
        print_error("Invalid algorithm: %s\n", algo_name);
        exit(1);
    }
}
