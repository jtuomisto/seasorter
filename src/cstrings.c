#include "cstrings-private.h"


/* Internal functions */
inline size_t first_not_sep(const char *str, size_t len) {
    size_t i = 0;
    while(i < len && strncmp(&str[i], PATH_SEP, 1) == 0) i++;
    return i;
}

inline size_t last_not_sep(const char *str, size_t len) {
    size_t i = len - 1;
    while(i > 0 && strncmp(&str[i], PATH_SEP, 1) == 0) i--;
    return i;
}


/* Public functions */
bool string_starts_with(const char *str, const char *start) {
    size_t str_len, start_len;

    if(str == NULL || start == NULL)
        return false;

    if(str == start)
        return true;

    str_len = strlen(str);
    start_len = strlen(start);

    if(start_len > str_len || start_len == 0 || str_len == 0)
        return false;
    else if(strncmp(str, start, start_len) == 0)
        return true;
    else
        return false;
}

bool string_ends_with(const char *str, const char *end) {
    size_t str_len, end_len;

    if(str == NULL || end == NULL)
        return false;

    if(str == end)
        return true;

    str_len = strlen(str);
    end_len = strlen(end);

    if(end_len > str_len || end_len == 0 || str_len == 0)
        return false;
    else if(strncmp(&str[str_len - end_len], end, end_len) == 0)
        return true;
    else
        return false;
}

char* string_concat(size_t count, ...) {
    size_t str_length = 0, len;
    va_list args;
    const char *temp;
    char *result;

    va_start(args, count);

    for(size_t i = 0; i < count; i++)
        str_length += strlen(va_arg(args, const char*));

    va_end(args);

    if(str_length == 0)
        return NULL;

    result = malloc(str_length * sizeof(char) + 1);

    if(result == NULL)
        return NULL;

    result[0] = '\0';

    va_start(args, count);

    for(size_t i = 0; i < count; i++) {
        temp = va_arg(args, const char*);
        len = strlen(temp);

        if(len == 0)
            continue;

        strcat(result, temp);
    }

    va_end(args);

    return result;
}

char* path_join(size_t count, ...) {
    size_t str_length = 0, first, last, len;
    va_list args;
    const char *temp;
    char *result;

    va_start(args, count);

    for(size_t i = 0; i < count; i++)
        str_length += strlen(va_arg(args, const char*));

    va_end(args);

    if(str_length == 0)
        return NULL;

    result = malloc((str_length + (count - 1)) * sizeof(char) + 1);

    if(result == NULL)
        return NULL;

    result[0] = '\0';

    va_start(args, count);

    for(size_t i = 0, real_i = 0; i < count; i++) {
        temp = va_arg(args, const char*);
        len = strlen(temp);

        if(len == 0)
            continue;

        first = first_not_sep(temp, len);
        last = last_not_sep(temp, len);

        if(first == len)
            continue;

        if(real_i == 0 && first > 0)
            first--;

        // strncat always appends null character, even if source is truncated
        strncat(result, &temp[first], last - first + 1);

        if(i < count - 1)
            strcat(result, PATH_SEP);

        real_i++;
    }

    va_end(args);

    return result;
}
