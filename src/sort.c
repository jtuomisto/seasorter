#include "sort-private.h"


/* Internal functions */
inline void swap(void *lhs, void *rhs, size_t size) {
    char *a = lhs, *b = rhs, temp;

    do {
        temp = *a;
        *a++ = *b;
        *b++ = temp;
    } while(--size > 0);
}


size_t quicksort_part(void *array, size_t low, size_t high, size_t size, const comparator_func_t comparator, const callback_func_t callback, void *cbdata) {
    void *pivot = R_P(array, low, size);
    size_t i = low - 1, j = high + 1;

    for(;;) {
        do {
            i++;
        } while(comparator(R_P(array, i, size), pivot) < 0);

        do {
            j--;
        } while(comparator(R_P(array, j, size), pivot) > 0);

        if(i >= j)
            return j;

        swap(R_P(array, i, size), R_P(array, j, size), size);
        CALLBACK(callback, cbdata, false);
    }
}

void quicksort_impl(void *array, size_t low, size_t high, size_t size, const comparator_func_t comparator, const callback_func_t callback, void *cbdata) {
    size_t pivot;

    if(low >= high)
        return;

    pivot = quicksort_part(array, low, high, size, comparator, callback, cbdata);
    quicksort_impl(array, low, pivot, size, comparator, callback, cbdata);
    quicksort_impl(array, pivot + 1, high, size, comparator, callback, cbdata);
}


void insertionsort_impl(void *array, size_t start, size_t length, size_t size, const comparator_func_t comparator, const callback_func_t callback, void *cbdata) {
    for(size_t i = start + 1, j = i; i < length; j = ++i) {
        while(j > start && comparator(R_P(array, j, size), R_P(array, j - 1, size)) < 0) {
            swap(R_P(array, j, size), R_P(array, j - 1, size), size);
            j--;
            CALLBACK(callback, cbdata, false);
        }
    }
}


size_t heap_leaf_search(void *array, size_t index, size_t length, size_t size, const comparator_func_t comparator) {
    while(RIGHT_CHILD(index) < length) {
        if(comparator(R_P(array, RIGHT_CHILD(index), size), R_P(array, LEFT_CHILD(index), size)) > 0)
            index = RIGHT_CHILD(index);
        else
            index = LEFT_CHILD(index);
    }

    if(LEFT_CHILD(index) < length)
        index = LEFT_CHILD(index);

    return index;
}

void heap_sift_down(void *array, size_t index, size_t length, size_t size, const comparator_func_t comparator, const callback_func_t callback, void *cbdata) {
    size_t i = heap_leaf_search(array, index, length, size, comparator);
    void *temp = malloc(size);

    while(comparator(R_P(array, index, size), R_P(array, i, size)) > 0)
        i = PARENT(i);

    memcpy(temp, R_P(array, i, size), size);
    memcpy(R_P(array, i, size), R_P(array, index, size), size);
    CALLBACK(callback, cbdata, false);

    while(i > index) {
        swap(temp, R_P(array, PARENT(i), size), size);
        i = PARENT(i);
        CALLBACK(callback, cbdata, false);
    }

    free(temp);
}

void heapsort_impl(void *array, size_t length, size_t size, const comparator_func_t comparator, const callback_func_t callback, void *cbdata) {
    size_t i;

    for(i = PARENT(length - 1) + 1; i-- > 0;)
        heap_sift_down(array, i, length, size, comparator, callback, cbdata);

    for(i = length - 1; i > 0; i--) {
        swap(R_P(array, i, size), R_P(array, 0, size), size);
        CALLBACK(callback, cbdata, false);
        heap_sift_down(array, 0, i, size, comparator, callback, cbdata);
    }
}


void merge_split(void *original, void *copy, size_t index, size_t length, size_t size, const comparator_func_t comparator, const callback_func_t callback, void *cbdata) {
    size_t middle;

    if(length - index < 2)
        return;

    middle = (length + index) / 2;

    merge_split(copy, original, index, middle, size, comparator, callback, cbdata);
    merge_split(copy, original, middle, length, size, comparator, callback, cbdata);
    merge(copy, original, index, middle, length, size, comparator, callback, cbdata);
}

void merge(void *original, void *copy, size_t index, size_t middle, size_t length, size_t size, const comparator_func_t comparator, const callback_func_t callback, void *cbdata) {
    for(size_t i = index, j = i, k = middle; i < length; i++) {
        if(j < middle && (k >= length || comparator(R_P(original, j, size), R_P(original, k, size)) <= 0))
            memcpy(R_P(copy, i, size), R_P(original, j++, size), size);
        else
            memcpy(R_P(copy, i, size), R_P(original, k++, size), size);

        CALLBACK(callback, cbdata, false);
    }
}


void introsort_impl(void *array, size_t depth, size_t length, size_t size, const comparator_func_t comparator, const callback_func_t callback, void *cbdata) {
    size_t pivot = quicksort_part(array, 0, length - 1, size, comparator, callback, cbdata);
    size_t slice = pivot + 1;
    size_t slice_len = length - slice;

    if(length <= 1) {
        return;
    }
    else if(depth == 0) {
        heapsort_impl(array, length, size, comparator, callback, cbdata);
    }
    else {
        introsort_impl(array, depth - 1, slice, size, comparator, callback, cbdata);
        introsort_impl(R_P(array, slice, size), depth - 1, slice_len, size, comparator, callback, cbdata);
    }
}


/* Public functions */
void shuffle(void *array, size_t length, size_t size) {
    size_t index;

    for(size_t i = 0; i < length - 2; i++) {
        index = (rand() % (length - i)) + i;
        swap(R_P(array, i, size), R_P(array, index, size), size);
    }
}


void bubblesort(void *array, size_t length, size_t size, const comparator_func_t comparator, const callback_func_t callback, void *cbdata) {
    bool swapped;
    size_t i;

    do {
        swapped = false;

        for(i = 1; i < length; i++) {
            if(comparator(R_P(array, i - 1, size), R_P(array, i, size)) > 0) {
                swap(R_P(array, i, size), R_P(array, i - 1, size), size);
                swapped = true;
                CALLBACK(callback, cbdata, false);
            }
        }
    } while(swapped);

    CALLBACK(callback, cbdata, true);
}


void quicksort(void *array, size_t length, size_t size, const comparator_func_t comparator, const callback_func_t callback, void *cbdata) {
    quicksort_impl(array, 0, length - 1, size, comparator, callback, cbdata);
    CALLBACK(callback, cbdata, true);
}


void shellsort(void *array, size_t length, size_t size, const comparator_func_t comparator, const callback_func_t callback, void *cbdata) {
    // Marcin Ciura's gap sequence has the best known performance, some people say
    static size_t ciura_gaps[] = {1577, 701, 301, 132, 57, 23, 10, 4, 1};
    size_t gap, i, j, k;
    void *temp = malloc(size);

    for(i = 0; i < 9; i++) {
        gap = ciura_gaps[i];

        for(j = gap; j < length; j++) {
            memcpy(temp, R_P(array, j, size), size);

            for(k = j; k >= gap && comparator(R_P(array, k - gap, size), temp) > 0; k -= gap) {
                memcpy(R_P(array, k, size), R_P(array, k - gap, size), size);
                CALLBACK(callback, cbdata, false);
            }

            memcpy(R_P(array, k, size), temp, size);
            CALLBACK(callback, cbdata, false);
        }
    }

    free(temp);
    CALLBACK(callback, cbdata, true);
}


void insertionsort(void *array, size_t length, size_t size, const comparator_func_t comparator, const callback_func_t callback, void *cbdata) {
    insertionsort_impl(array, 0, length, size, comparator, callback, cbdata);
    CALLBACK(callback, cbdata, true);
}


void oddevensort(void *array, size_t length, size_t size, const comparator_func_t comparator, const callback_func_t callback, void *cbdata) {
    bool swapped;
    size_t i;

    do {
        swapped = false;

        for(i = 0; i < length - 1; i += 2) {
            if(comparator(R_P(array, i, size), R_P(array, i + 1, size)) > 0) {
                swap(R_P(array, i, size), R_P(array, i + 1, size), size);
                swapped = true;
                CALLBACK(callback, cbdata, false);
            }
        }

        for(i = 1; i < length - 1; i += 2) {
            if(comparator(R_P(array, i, size), R_P(array, i + 1, size)) > 0) {
                swap(R_P(array, i, size), R_P(array, i + 1, size), size);
                swapped = true;
                CALLBACK(callback, cbdata, false);
            }
        }
    } while(swapped);

    CALLBACK(callback, cbdata, true);
}


void cocktailsort(void *array, size_t length, size_t size, const comparator_func_t comparator, const callback_func_t callback, void *cbdata) {
    size_t start = 0, end = length - 2, new_start, new_end, i;

    while(start <= end) {
        new_start = end;
        new_end = start;

        for(i = start; i <= end; i++) {
            if(comparator(R_P(array, i, size), R_P(array, i + 1, size)) > 0) {
                swap(R_P(array, i, size), R_P(array, i + 1, size), size);
                new_end = i;
                CALLBACK(callback, cbdata, false);
            }
        }

        end = new_end - 1;

        for(i = end + 1; i-- > start;) {
            if(comparator(R_P(array, i, size), R_P(array, i + 1, size)) > 0) {
                swap(R_P(array, i, size), R_P(array, i + 1, size), size);
                new_start = i;
                CALLBACK(callback, cbdata, false);
            }
        }

        start = new_start + 1;
    }

    CALLBACK(callback, cbdata, true);
}


void combsort(void *array, size_t length, size_t size, const comparator_func_t comparator, const callback_func_t callback, void *cbdata) {
    static float shrink_factor = 1.3;  // Some people suggest this is optimal
    size_t gap = length, i;
    bool sorted = false;

    while(!sorted) {
        gap /= shrink_factor;

        if(gap > 1) {
            sorted = false;
        }
        else {
            gap = 1;
            sorted = true;
        }

        for(i = 0; i + gap < length; i++) {
            if(comparator(R_P(array, i, size), R_P(array, i + gap, size)) > 0) {
                swap(R_P(array, i, size), R_P(array, i + gap, size), size);
                sorted = false;
                CALLBACK(callback, cbdata, false);
            }
        }
    }

    CALLBACK(callback, cbdata, true);
}


void cyclesort(void *array, size_t length, size_t size, const comparator_func_t comparator, const callback_func_t callback, void *cbdata) {
    size_t i, j, pos;
    void *temp = malloc(size);

    for(i = 0, pos = 0; i < length - 1; pos = ++i) {
        memcpy(temp, R_P(array, i, size), size);

        for(j = i + 1; j < length; j++) {
            if(comparator(temp, R_P(array, j, size)) > 0)
                pos++;
        }

        if(pos == i)
            continue;

        while(comparator(temp, R_P(array, pos, size)) == 0)
            pos++;

        swap(R_P(array, pos, size), temp, size);
        CALLBACK(callback, cbdata, false);

        while(pos != i) {
            pos = i;

            for(j = i + 1; j < length; j++) {
                if(comparator(temp, R_P(array, j, size)) > 0)
                    pos++;
            }

            while(comparator(temp, R_P(array, pos, size)) == 0)
                pos++;

            swap(R_P(array, pos, size), temp, size);
            CALLBACK(callback, cbdata, false);
        }
    }

    free(temp);
    CALLBACK(callback, cbdata, true);
}


void heapsort(void *array, size_t length, size_t size, const comparator_func_t comparator, const callback_func_t callback, void *cbdata) {
    heapsort_impl(array, length, size, comparator, callback, cbdata);
    CALLBACK(callback, cbdata, true);
}


void selectionsort(void *array, size_t length, size_t size, const comparator_func_t comparator, const callback_func_t callback, void *cbdata) {
    size_t temp;

    for(size_t i = 0, j = 0; i < length - 1; i++) {
        temp = i;

        for(j = i + 1; j < length; j++) {
            if(comparator(R_P(array, j, size), R_P(array, temp, size)) < 0)
                temp = j;
        }

        if(temp != i) {
            swap(R_P(array, i, size), R_P(array, temp, size), size);
            CALLBACK(callback, cbdata, false);
        }
    }

    CALLBACK(callback, cbdata, true);
}


void mergesort(void *array, size_t length, size_t size, const comparator_func_t comparator, const callback_func_t callback, void *cbdata) {
    void *copy = malloc(length * size);

    memcpy(copy, array, length * size);
    merge_split(array, copy, 0, length, size, comparator, callback, cbdata);
    free(copy);
    CALLBACK(callback, cbdata, true);
}


void introsort(void *array, size_t length, size_t size, const comparator_func_t comparator, const callback_func_t callback, void *cbdata) {
    // Somewhat arbitary number, also used in GNU C++ library
    size_t depth = 2 * log2l(length);

    introsort_impl(array, depth, length, size, comparator, callback, cbdata);
    CALLBACK(callback, cbdata, true);
}
