seasorter
=========

**Visualize sorting algorithms with images.**

### Dependencies:

- meson
- ImageMagick 7 libraries & development headers

### Build instructions:

1. `meson build`
2. `cd build`
3. `ninja`

### Examples:

[Link to examples](https://gitlab.com/jtuomisto/seasorter/tree/master/example)
