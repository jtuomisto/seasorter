#!/bin/bash

rate=40


if [ -z "$1" ]; then
    printf "At least one directory must given\n"
    exit 1
fi


while [ -n "$1" ]; do
    frame_list="$(ls -1 "${1}"/*.png 2> /dev/null)"

    if [ $? -ne 0 ]; then
        printf "No frames in directory $1\n"
        continue
    fi

    frames="$(wc -l <<< "$frame_list")"

    ffmpeg -r $rate -f image2 -start_number 1 -i "${1}/%d.png" -vframes "$frames" \
           -vf "pad=ceil(iw/2)*2:ceil(ih/2)*2" \
           -vcodec libx264 -crf 17 -pix_fmt yuv420p "${1}.mp4"

    shift 1;
done
