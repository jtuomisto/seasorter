#!/bin/bash

n_frames=400
build_dir="build"
binary_name="seasorter"
frames_dir="/tmp/frames"
sorts=("bubble" "quick" "shell" "insertion" "oddeven" "cocktail"
       "comb" "cycle" "heap" "selection" "merge" "intro")

input_image="$1"
script_path="$(dirname "$(readlink -f "$0")")"
binary="${script_path}/../${build_dir}/${binary_name}"


if [ ! -x "$binary" ]; then
    printf "This script expects the %s binary to be at: %s\n" "$binary_name" "$binary"
    exit 1
fi

if [ -z "$input_image" ]; then
    printf "No input image given\n"
    exit 1
fi


run_cmd() {
    "$@"

    if [ $? -ne 0 ]; then
        printf "Command failed: %s\n" "${@}"
        exit 1
    fi
}


for sort in ${sorts[@]}; do
    def_output_dir="${frames_dir}/${sort}_pos"
    col_output_dir="${frames_dir}/${sort}_color"

    mkdir -p "$def_output_dir"
    mkdir -p "$col_output_dir"

    run_cmd "$binary" -s -o "$def_output_dir" -f "$n_frames" -a "$sort" "$input_image"
    run_cmd "$binary" -c -s -o "$col_output_dir" -f "$n_frames" -a "$sort" "$input_image"
done
