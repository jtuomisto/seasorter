#ifndef SORT_H_INCLUDED
#define SORT_H_INCLUDED

#include <stddef.h>
#include <stdbool.h>


typedef void (*callback_func_t)(void *cbdata, bool is_finished);
typedef int (*comparator_func_t)(void *lhs, void *rhs);
typedef void (*sort_func_t)(void *array, size_t length, size_t size, const comparator_func_t comparator, const callback_func_t callback, void *cbdata);


void shuffle(void *array, size_t length, size_t size);

void bubblesort(void *array, size_t length, size_t size, const comparator_func_t comparator, const callback_func_t callback, void *cbdata);
void quicksort(void *array, size_t length, size_t size, const comparator_func_t comparator, const callback_func_t callback, void *cbdata);
void shellsort(void *array, size_t length, size_t size, const comparator_func_t comparator, const callback_func_t callback, void *cbdata);
void insertionsort(void *array, size_t length, size_t size, const comparator_func_t comparator, const callback_func_t callback, void *cbdata);
void oddevensort(void *array, size_t length, size_t size, const comparator_func_t comparator, const callback_func_t callback, void *cbdata);
void cocktailsort(void *array, size_t length, size_t size, const comparator_func_t comparator, const callback_func_t callback, void *cbdata);
void combsort(void *array, size_t length, size_t size, const comparator_func_t comparator, const callback_func_t callback, void *cbdata);
void cyclesort(void *array, size_t length, size_t size, const comparator_func_t comparator, const callback_func_t callback, void *cbdata);
void heapsort(void *array, size_t length, size_t size, const comparator_func_t comparator, const callback_func_t callback, void *cbdata);
void selectionsort(void *array, size_t length, size_t size, const comparator_func_t comparator, const callback_func_t callback, void *cbdata);
void mergesort(void *array, size_t length, size_t size, const comparator_func_t comparator, const callback_func_t callback, void *cbdata);
void introsort(void *array, size_t length, size_t size, const comparator_func_t comparator, const callback_func_t callback, void *cbdata);


#endif
