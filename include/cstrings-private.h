#ifndef CSTRINGS_PRIVATE_H_INCLUDED
#define CSTRINGS_PRIVATE_H_INCLUDED

#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include "cstrings.h"


inline size_t first_not_sep(const char *str, size_t len);
inline size_t last_not_sep(const char *str, size_t len);


#endif
