#ifndef SEASORTER_H_INCLUDED
#define SEASORTER_H_INCLUDED

#include <stddef.h>
#include <stdbool.h>
#include "sort.h"


typedef struct {
    char *name;
    sort_func_t func;
} sort_algo;

typedef struct {
    bool separate;
    bool colors;
    size_t frames;
    const char *input;
    const char *output;
    const sort_algo *algo;
} sort_options;


void seasorter_init();
const sort_algo* seasorter_get_algo(const char *algo);
bool seasorter_sort(const sort_options *options);


#endif
