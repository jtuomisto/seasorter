#ifndef SORT_PRIVATE_H_INCLUDED
#define SORT_PRIVATE_H_INCLUDED

#include <stdlib.h>
#include <math.h>
#include <string.h>
#include "sort.h"

// Calculate correct pointer
#define R_P(array, i, sz) ((char*) array + (i) * (sz))
#define CALLBACK(cb, data, end) do { if(cb != NULL) cb(data, end); } while(0)

#define PARENT(i) ((i - 1) / 2)
#define LEFT_CHILD(i) (2 * i + 1)
#define RIGHT_CHILD(i) (2 * i + 2)


inline void swap(void *lhs, void *rhs, size_t size);

size_t quicksort_part(void *array, size_t low, size_t high, size_t size, const comparator_func_t comparator, const callback_func_t callback, void *cbdata);
void quicksort_impl(void *array, size_t low, size_t high, size_t size, const comparator_func_t comparator, const callback_func_t callback, void *cbdata);

void insertionsort_impl(void *array, size_t start, size_t length, size_t size, const comparator_func_t comparator, const callback_func_t callback, void *cbdata);

size_t heap_leaf_search(void *array, size_t index, size_t length, size_t size, const comparator_func_t comparator);
void heap_sift_down(void *array, size_t index, size_t length, size_t size, const comparator_func_t comparator, const callback_func_t callback, void *cbdata);
void heapsort_impl(void *array, size_t length, size_t size, const comparator_func_t comparator, const callback_func_t callback, void *cbdata);

void merge_split(void *original, void *copy, size_t index, size_t length, size_t size, const comparator_func_t comparator, const callback_func_t callback, void *cbdata);
void merge(void *original, void *copy, size_t index, size_t middle, size_t length, size_t size, const comparator_func_t comparator, const callback_func_t callback, void *cbdata);

void introsort_impl(void *array, size_t depth, size_t length, size_t size, const comparator_func_t comparator, const callback_func_t callback, void *cbdata);


#endif
