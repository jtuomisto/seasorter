#ifndef CSTRINGS_H_INCLUDED
#define CSTRINGS_H_INCLUDED

#include <stddef.h>
#include <stdbool.h>

#ifndef PATH_SEP
#define PATH_SEP "/"
#endif


bool string_starts_with(const char *str, const char *start);
bool string_ends_with(const char *str, const char *end);
char* string_concat(size_t count, ...);
char* path_join(size_t count, ...);


#endif
