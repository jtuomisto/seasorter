#ifndef SEASORTER_PRIVATE_H_INCLUDED
#define SEASORTER_PRIVATE_H_INCLUDED

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include <errno.h>
#include <sys/stat.h>
#include <MagickWand/MagickWand.h>
#include "seasorter.h"
#include "cstrings.h"

#define COMPARE(a, b) (a > b ? 1 : (a < b ? -1 : 0))
#define DIR_PERM (S_IRWXU | S_IRGRP | S_IXGRP | S_IROTH | S_IXOTH)  // 0755


typedef uint32_t num_t;

typedef struct {
    MagickWand *result;
    const char *output;
    num_t *pixel_data;
    size_t *pixel_pos;
    size_t width, height, counter, new_frame, frame;
    bool separate;
    bool success;
} callback_data;


const sort_algo seasorter_algos[];


void magick_fail(MagickWand *wand);

inline num_t get_clamped_color(MagickRealType color);
inline MagickRealType get_quantum_color(num_t color);
inline num_t pack_clamped(num_t r, num_t g, num_t b);
inline void unpack_clamped(num_t color, num_t *r, num_t *g, num_t *b);

int index_comparator(void *lhs, void *rhs);
int color_comparator(void *lhs, void *rhs);

void add_callback(void *data, bool is_finished);
void img_callback(void *data, bool is_finished);

num_t* get_pixel_data(const char *input, size_t *w, size_t *h);
size_t* get_pixel_pos(size_t w, size_t h);

size_t calculate_operations(void *array, size_t length, size_t size, comparator_func_t comparator, sort_func_t algo);

bool write_gif(const char *output, MagickWand *wand);
bool write_png(const char *directory, size_t frame, MagickWand *wand);

bool create_dir(const char *path);

void seasorter_cleanup();


#endif
